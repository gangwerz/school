module.exports = (grunt) => {
	require("load-grunt-tasks")(grunt);

	grunt.initConfig({
		//Minify Javascript using uglify
		uglify: {
			files: {
				src:		"scripts/src/*.js",	// source files mask
				dest:		"scripts/min/",			// destination folder
				expand:		true,					// allow dynamic building
				flatten:	true,					// remove all unnecessary nesting
				ext: 		".min.js"				// replace .js to .min
			}
		},
		watch: {
			js:  { files: "js/*.js", tasks: [ "uglify" ] }
		}
	});

	// load plugins
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-uglify");

	// register at least this one task
	grunt.registerTask("default", ["uglify"]);
};
