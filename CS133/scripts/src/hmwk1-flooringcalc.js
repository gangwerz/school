window.onload = function() {
	/**
	 * Computes and Displays the Square Footage
	 */
	document.getElementById("computebtn").onclick = function () {
		var length = document.querySelector("#length").value;
		var width = document.querySelector("#width").value;
		var overage = document.querySelector("#overage").value;

		var sqft = computeFeet(length, width, overage);

		document.querySelector("#result_ft").value = sqft + " ft^2";
		document.querySelector("#result_yrd").value = Math.ceil(sqft/9) + " yrd^2";
	};

	/**
	 * Resets all fields
	 */
	document.getElementById("resetbtn").onclick = function () {
		document.querySelector("#result_ft").value = "";
		document.querySelector("#result_yrd").value = "";
		document.querySelector("#length").value = "";
		document.querySelector("#width").value = "";
		document.querySelector("#overage").value = "";
	};
};

/**
 * Computes the Square footage in Feet
 * @param  {int} length		-> Length of the room
 * @param  {int} width  	-> Width of the room
 * @param  {int|float} ovr	-> Percent of acceptable overage
 * @return {int}        	=> The Square Footage
 */
function computeFeet(length, width, ovr) {
	var sqft = length*width;

	if (ovr >= 1) {
		sqft += sqft*(ovr/100);
	} else if (ovr > 0 && ovr < 1) {
		sqft += sqft*ovr
	}

	return sqft;
}
