# Computer Science 133j

## Overview

Herein lies all the code I wrote for my Intro to Javascript class taken during the 2015-2016 school year. While the class was inteded to teach basic JS in HTML pages, I used it to experiment with the NodeJS ecosystem, namely Babel, Grunt, and React. I originally intended to create a React-backed app, but did not have the time between school and work to realize that goal. What remained was a project written in the ECMA 2015 standard, transpiled to the default target by Babel, and minified with Uglify to experiment with Grunt. Babel has been removed, as the features I used in the various assignments are now commonplace in modern web browsers.

## Changes

* package.json
    * Updated to reflect latest
    * Removed Babel Dependancies, targeting current ECMA standard
* gruntfile.js
    * Fixed uglify script to output \*.min.js instead of \*.min
    * Removed Babel task
* scripts/src
    * Renamed ECMAscript (\*.es) files to Javascript (\*.js) files
* .gitignore
    * Added to remove the inclusion of node_modules in the repo, as the packages used are build dependancies and would not be needed for deployments.

## Homework 1 -  Flooring (Square Footage) Calculator

### Overview

This project is a square-footage calculator web-app that took in three values (length, width, and overage) and yielded the square footage and square yardage.

### Changes

* HTML
    * Inserted a line break between the Feet and Yards result fields
    * Renamed `result` fields to clarify the field names
    * Added placeholder text to result fields
    * Changed `overage` placeholder to reflect acceptance of floats
* Javascript
    * Renamed `result` field queries to match HTML changes
    * Implemented `reset` button functionality
    * Added support for floats in overage field
    * Changed `Math.round()` to `Math.ceil()` to round correctly for this type of calculator.

## Homework 2 - Timer

### Overview

A simple stop-watch style timer with start, stop, resumption, lap recording, and reset functionality.

### Changes

* HTML
    * Fixed `<script>` path for current Grunt output
    * Added `<div id=output>` to output area to allow for single CSS file useage
* Javascript
    * Created `startTimer()` and `clearTimer()` functions to clean up code
    * Removed unnecessary time global variable
    * Fixed `lap` button functionality
