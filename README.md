# School Work

## Content of this Repo

As the repo name may suggest, this is a collection of my school work code I still have, and am allowed to post. 

Most of the code is from my intermediate level computer science classes. This is due to all my upper level course professors who are currently still teaching, and have expressed their displeasure of both present and former students publicly posting code for their assignments.

## Why are there updates if this is old code?

I am currently going through each of my old projects to update them with the experience I have garnered since my time taking these courses. I have commited the projects in their initial state and you can go through the revision history to view my changes. While I will be changing the code, I aim to leave it close to it's original state. However I want it to be cleaner and more of a reflection of my current skill level than they would otherwise present.

*Change logs for each project are available in the README of each respective class folders, broken down for the whole class directory, as well as individual assignments.*

## Repo Construction


I am using the popular [git-flow](https://github.com/nvie/gitflow) branching model for going through the projects, with each class making up a `feature/*` branch.

The `feature/*` and `develop` branches will be merged to `master` intermittently, but please look at the branches if you want to see the most up to date status.
